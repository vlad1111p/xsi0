package com.sda;

import java.util.Scanner;

public class Game {
    int moves;
    boolean haswinner;

    public Game() {
        this.moves = 0;
        this.haswinner = false;
    }

    public void incrementMove(){
        this.moves=this.moves+1;
    }

    public int getMoves() {
        return moves;
    }

    public void setMoves(int moves) {
        this.moves = moves;
    }

    public String[][] generateBoard(){
        String[][] board = new String[3][3];

        for(int i=0; i< board.length; i++){
            for(int j=0; j < board.length; j++){
                board[i][j] = "_";
            }
        }
        return board;
    }

    public void printBoard(String[][] board){

        for(int i=0; i< board.length; i++){
            for(int j=0; j < board.length; j++){
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }
    }
    public void setSymbol(String[][] board, Scanner sc){
        System.out.println("choose your symbol=");
        String symbol = sc.next();


        Position symbolPosition = new Position();
        symbolPosition.setPos(board,sc);
        symbolPosition.isPositionValid(board, sc);

        board[symbolPosition.row][symbolPosition.column] = symbol;
        this.haswinner=isWinner(board,symbol);

        incrementMove();
        printBoard(board);
        if (this.haswinner)
            System.out.println(symbol + " has won");
    }

    public boolean isWinner(String[][] board, String symbol){
        boolean isTrue1 = true;
        boolean isTrue = true;
        for (int row = 0; row < board.length; row ++){

            for(int col = 0; col < board.length; col ++){
                if(board[row][col].equals(symbol)){
                    isTrue = isTrue && true;
                } else{
                    isTrue = false;
                }
            }
            if(isTrue == true){
                return true;
            }
        }
        isTrue = true;
        for (int col = 0; col < board.length; col++){

            for(int row = 0; row < board.length; row++){
                if(board[row][col].equals(symbol)){
                    isTrue = isTrue && true;
                } else{
                    isTrue = false;
                }
            }
            if(isTrue == true){
                return true;            }
        }


        for (int col = 0; col < board.length; col++){
            if(board[col][col].equals(symbol)){
                isTrue1 = isTrue1 && true;
            }else{
                isTrue1=false;
            }
        }
        if(isTrue1){
           return true;        }

        int row=2;
        isTrue1 = true;
        for (int col = 0; col < board.length; col++){
            if(board[col][row].equals(symbol)){
                isTrue1 = isTrue1 && true;
            }
            else{
                isTrue1=false;
            }
            row--;
        }
        if(isTrue1){
            return true;        }

        return false;    }


}