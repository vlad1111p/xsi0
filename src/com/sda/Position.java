package com.sda;


import java.util.Scanner;

public class Position {
    public int row;
    public int column;

    public Position(int row, int column){
        this.row = row;
        this.column = column;
    }
    public Position(){

    }

    private void isPositionOccupied(String[][] board, Scanner sc){
        while(!board[this.row][this.column].equals("_")){
            System.out.println("Invalid position! Please set symbol again!");
            System.out.println("Row: ");
            this.row = sc.nextInt();
            System.out.println("Column: ");
            this.column = sc.nextInt();
        }
    }

    private void isIndexInRange(String[][] board, Scanner sc){
        while(this.column > board.length || this.row > board.length){
            System.out.println("Enter index in range 0 - " + (board.length - 1)+ ".");
            System.out.println("Row: ");
            this.row = sc.nextInt();
            System.out.println("Column: ");
            this.column = sc.nextInt();
        }
    }
    public void setPos(String[][] board, Scanner sc){

            System.out.println("Enter index in range 0 - " + (board.length - 1)+ ".");
            System.out.println("Row: ");
            this.row = sc.nextInt();
            System.out.println("Column: ");
            this.column = sc.nextInt();

    }


    public void isPositionValid(String[][] board, Scanner sc){
        isIndexInRange(board, sc);
        isPositionOccupied(board, sc);
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }
}
