package com.sda;

import java.util.Scanner;

/**
 Method to generate game table
 Method for player to choose symbol x or 0 ( if invalid position => error message )
 Method to determine if there are 3 simultaneous symbols on a row,column or  2x diagonal
 */

public class Main {

    public static void main(String[] args) {

        Game game=new Game();
        String[][] board = game.generateBoard();
        Scanner sc = new Scanner(System.in).useDelimiter("\\n");
        game.printBoard(board);



        //<9||!game.haswinner
        while(!game.haswinner&&game.getMoves()<9) {

            game.setSymbol(board, sc);


        }
    }






}
